# TP_ROBOT_PINNAU_TAWATA_ESCALANTE_JUNGHEIM


PROGRAMA 1

Diagrama de estados del funcionamiento del robot:

En este diagrama se graficaron dos estados uno llamado "AVANZAR" y el otro "ESQUIVAR". El primer estado lo que hace es hacer girar las dos ruedas hacia adelante. El segundo estado lo que hace es esquivar un objeto y vuelve a tomar la posicion del inicio. Los eventos disparadores son la deteccion de objeto para pasar del estado 1 al 2. El otro evento disparador es no detectar ningun objeto este va del estado 2 al 1.

Videos del comportamiento:

https://web.microsoftstream.com/video/ce6a2902-d627-41fe-a141-425b5ca3b92d

https://web.microsoftstream.com/video/1f1fb0a8-0e09-4ff2-b4b3-791e9a837273

Codigo del funcionamiento:

```
#define IN1 8
#define IN2 9
#define IN3 10
#define IN4 11

#define TRIGGER 7
#define ECHO 6

#define ESTADO_0 0
#define ESTADO_1 1 

int estado = ESTADO_0;
long pulso_alto;
long valor;

void setup() {
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  
  pinMode(TRIGGER, OUTPUT);
  pinMode(ECHO, INPUT);

  Serial.begin(9600);

}

void loop() 
{
  digitalWrite (TRIGGER, LOW);
  delayMicroseconds(20);
  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(20);         
  digitalWrite(TRIGGER, LOW);

  pulso_alto = pulseIn(ECHO, HIGH);
  valor = pulso_alto/59;
  delay(100);

  Serial.println(valor);
  
  if(valor >= 30){estado = ESTADO_0; }
  else{estado = ESTADO_1; }
   
  switch (estado)
  {
    case ESTADO_0:
                digitalWrite(IN1,LOW);
                analogWrite(IN2,200);
                analogWrite(IN3,100);
                digitalWrite(IN4,LOW);
                Serial.println(estado);
                
                if(valor >= 10){estado = ESTADO_0; }
                if(valor < 10){estado = ESTADO_1; }
                break;
  
  
    case ESTADO_1:
                digitalWrite(IN1,LOW);
                digitalWrite(IN2,LOW);
                digitalWrite(IN3,LOW);
                digitalWrite(IN4,LOW);
                
                digitalWrite(IN1,LOW);
                analogWrite(IN2,200);                      /*GIRA HACIA LA IZQUIERDA*/
                digitalWrite(IN3,LOW);       
                digitalWrite(IN4,LOW);

                digitalWrite(IN1,LOW);
                digitalWrite(IN2,LOW);
                digitalWrite(IN3,LOW);
                digitalWrite(IN4,LOW);

                digitalWrite(IN1,LOW);
                analogWrite(IN2,200);
                analogWrite(IN3,100);                      /*AVANZA*/
                digitalWrite(IN4,LOW);
                
                delay(2000);

                digitalWrite(IN1,LOW);
                digitalWrite(IN2,LOW);
                digitalWrite(IN3,LOW);
                digitalWrite(IN4,LOW);

                digitalWrite(IN1,LOW);
                digitalWrite(IN2,LOW);
                analogWrite(IN3,200);                    /*GIRA HACIA LA DERECHA*/
                digitalWrite(IN4,LOW);

                digitalWrite(IN1,LOW);
                digitalWrite(IN2,LOW);
                digitalWrite(IN3,LOW);
                digitalWrite(IN4,LOW);

                digitalWrite(IN1,LOW);
                analogWrite(IN2,200);
                analogWrite(IN3,100);                      /*AVANZA*/
                digitalWrite(IN4,LOW);

                delay(2000);

                digitalWrite(IN1,LOW);
                digitalWrite(IN2,LOW);
                digitalWrite(IN3,LOW);
                digitalWrite(IN4,LOW);

                digitalWrite(IN1,LOW);
                digitalWrite(IN2,LOW);
                analogWrite(IN3,200);                   /*GIRA HACIA LA DERECHA*/
                digitalWrite(IN4,LOW);
                
                digitalWrite(IN1,LOW);
                digitalWrite(IN2,LOW);
                digitalWrite(IN3,LOW);
                digitalWrite(IN4,LOW);

                digitalWrite(IN1,LOW);
                analogWrite(IN2,200);
                analogWrite(IN3,100);                    /*AVANZA*/
                digitalWrite(IN4,LOW);

                delay(2000);

                digitalWrite(IN1,LOW);
                digitalWrite(IN2,LOW);
                digitalWrite(IN3,LOW);
                digitalWrite(IN4,LOW);

                digitalWrite(IN1,LOW);
                analogWrite(IN2,200);                    /*GIRA HACIA LA IZQUIERDA*/
                digitalWrite(IN3,LOW);
                digitalWrite(IN4,LOW);
                
                if(valor >= 10){estado = ESTADO_0; }
                if(valor < 10){estado = ESTADO_1; }
                break;
  }            
 
    
 
}




