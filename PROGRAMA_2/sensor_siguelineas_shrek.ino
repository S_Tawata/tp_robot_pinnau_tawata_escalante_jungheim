
#define IN1 8
#define IN2 9
#define IN3 10
#define IN4 11

#define sensi 7
#define sensd 6

#define AVANZAR 0 //definimos los estados
#define GIRARI 1 
#define GIRARD 2
#define FRENAR 3

int estado = case; 
int lectura_sensi; //las variables son int porque no nos interesa saber cuanto sensa cada sensor, solo si sensa o no
int lectura_sensd;

void setup() { //declaramos los pines de salida y entrada
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  
  pinMode(lectura_sensi, INPUT);
  pinMode(lectura_sensd, INPUT);

  Serial.begin(9600);

}

void loop() 
{
  lectura_sensi = digitalRead(sensi); // almacena la lectura del sensor izquierdo
  lectura_sensd = digitalRead(sensd); // almacena la lectura del sensor derecho
 
  Serial.println("El valor del sensor izquierdo es "); //vemos que la lectura del sensor izquierdo
  Serial.println(lecturaSensorIzq);
 
  Serial.println("El valor del sensor derecho es "); //vemos que la lectura del sensor derecho
  Serial.println(lecturaSensorDer);
  
  if(lectura_sensi == 0 && lectura_sensd == 0) //si ninguno los 2 sensores sensa nada pasa al estado AVANZAR
  {
    estado = AVANZAR;
  }
  
  if(lectura_sensi == 1 && lectura_sensd == 0) //si el sensor de la izquierda sensa algo y el de la derecha no pasa al estado GIRARI
  {
    estado = GIRARI;
  }
  
  if(lectura_sensi == 0 && lectura_sensd == 1) //si el sensor de la izquierda no sensa nada y el de la derecha si pasa al estado GIRARD
  {
    estado = GIRARD;
  }
  
  if(lectura_sensi == 1 && lectura_sensd == 1) //si los 2 sensores sensan algo pasa al estado FRENAR
  {
    estado = FRENAR;
  }
   
  switch (estado) //estamos usando una máquina de 4 estados
  {
    case AVANZAR: //cuando se encuentra en este estado el robot avanza
                digitalWrite(IN1,LOW);
                analogWrite(IN2,HIGH);
                digitalWrite(IN3,HIGH);
                analogWrite(IN4,LOW);
                Serial.println(estado);
                break;
    
    case GIRARI://cuando se encuentra en este estado el robot gira hacia la izquierda
                digitalWrite(IN1,LOW);
                analogWrite(IN2,HIGH);
                digitalWrite(IN3,LOW);
                analogWrite(IN4,HIGH);
                Serial.println(estado);
                break;
                
    case GIRARD://cuando se encuentra en este estado el robot gira hacia la derecha
                digitalWrite(IN1,HIGH);
                analogWrite(IN2,LOW);
                digitalWrite(IN3,HIGH);
                analogWrite(IN4,LOW);
                Serial.println(estado);
                break;
  
  
    case FRENAR://cuando se encuentra en este estado el robot frena
                digitalWrite(IN1,LOW);
                digitalWrite(IN2,LOW);
                digitalWrite(IN3,LOW);
                digitalWrite(IN4,LOW);
                Serial.println(estado);
                break;
  }            
 
    
 
}
